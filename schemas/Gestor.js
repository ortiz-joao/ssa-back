const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt } = graphql;

module.exports = new GraphQLObjectType({
  name: 'Gestor',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(gestor) {
        return gestor.id
      }
    },
    usuario: {
      type: UsuarioType,
      resolve(parent) {
        return Gestor.findByPk(parent.id).then(gestor => {
          return gestor.gestUsuario()
        })
      }
    }
  })
})

const Gestor = require('../models/Gestor')
const UsuarioType = require('./Usuario')
