const graphql = require('graphql');
const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } = graphql;

module.exports = new GraphQLObjectType({
  name: 'Professor',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(professor) {
        return professor.id
      }
    },
    percursos: {
      type: GraphQLList(PercursoType),
      args: {
        id: {
          type: GraphQLInt
        },
        descricao: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Professor.findByPk(parent.id).then(professor => {
          return professor.getResponsavelPelosPercursos()
        })
      }
    },
    usuario: {
      type: UsuarioType,
      resolve(parent, args, context, info) {
          return Professor.findByPk(parent.id).then(professor => {
            return professor.getUsuario()
          })
      }
    },
    leciona: {
      type: GraphQLList(DisciplinaType),
      args: {
        id:{
          type: GraphQLInt
        },
        disciplina: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Professor.findByPk(parent.id).then(professor => {
          return professor.getDisciplinas({where: args})
        })
      }
    },
    orientandos: {
      type: GraphQLList(GrupoType),
      resolve(parent) {
        return Professor.findByPk(parent.id).then(professor => {
          return professor.getOrientandos()
        })
      }
    }
  })
})

const Professor = require("../models/Professor")
const GrupoType = require('./Grupo')
const UsuarioType = require('./Usuario')
const DisciplinaType = require('./Disciplina')
const PercursoType = require('./Percurso')
