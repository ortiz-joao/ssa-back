const graphql = require('graphql');
const { GraphQLList, GraphQLObjectType, GraphQLString, GraphQLInt } = graphql;

module.exports = new GraphQLObjectType({
  name: 'Grupo',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(grupo) {
        return grupo.id;
      }
    },
    alunos:{
      type: GraphQLList(AlunoType),
      resolve(parent, args, context, info) {
          return Grupo.findByPk(parent.id).then(grupo => {
            return grupo.getAlunos({where: args})
          })
      }
    },
    orientadores: {
      type: GraphQLList(ProfessorType),
      resolve(parent, args, context, info) {
          return Grupo.findByPk(parent.id).then(grupo => {
            return grupo.getOrientadores({where: args})
          })
      }
    }
  })
})


const AlunoType = require('./Aluno')
const ProfessorType = require('./Professor')
const Grupo = require('../models/Grupo')
