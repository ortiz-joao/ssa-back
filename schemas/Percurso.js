const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;

module.exports = new GraphQLObjectType({
  name: 'Percurso',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(percurso) {
        return percurso.id;
      }
    },
    nome: {
      type: GraphQLString,
      resolve(percurso) {
        return percurso.nome
      }
    },
    descricao: {
      type: GraphQLString,
      resolve(percurso) {
        return percurso.descricao
      }
    },
    atividadesDoPercurso: {
      type: GraphQLList(AtividadeType),
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString,
        },
        horarioInicio: {
          type: GraphQLDateTime,
        },
        horarioTermino: {
          type: GraphQLDateTime,
        }
      },
      resolve(parent, args) {
        return Percurso.findByPk(parent.id).then(percurso => {
          return percurso.getAtividadesDoPercurso({where: args})
        })
      }
    },
    professores: {
      type: GraphQLList(ProfessorType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Percurso.findByPk(parent.id).then(percurso => {
          return percurso.getResponsaveisDoPercurso({where: args})
        })
      }
    },
    alunos: {
      type: GraphQLList(AlunoType),
      args: {
        id: {
          type: GraphQLInt
        },
        turma: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Percurso.findByPk(parent.id).then(percurso => {
          return percurso.getAlunos({where: args})
        })
      }
    }
  })
})

const Percurso = require("../models/Percurso")
const ProfessorType = require('./Professor')
const AlunoType = require('./Aluno')
const AtividadeType = require('./Atividade')
