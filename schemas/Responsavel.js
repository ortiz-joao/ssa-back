const graphql = require('graphql');
const { GraphQLList, GraphQLObjectType, GraphQLInt, GraphQLString } = graphql;
const Responsavel = require("../models/Responsavel")

module.exports = new GraphQLObjectType({
  name: 'Responsavel',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(responsavel) {
        return responsavel.id
      }
    },
    usuario: {
      type: UsuarioType,
      resolve(parent, args, context, info) {
          return Responsavel.findByPk(parent.id).then(responsavel => {
            return responsavel.getUsuario()
          })
      }
    },
    responsavelPor: {
      type: GraphQLList(AlunoType),
      args: {
        id: {
          type: GraphQLInt
        },
        turma: {
          type: GraphQLString
        }
      },
      resolve(parent) {
        return Responsavel.findByPk(parent.id).then(responsavel => {
          return responsavel.getAlunos({where: args})
        })
      }
    }
  })
})

const AlunoType = require('./Aluno')
const UsuarioType = require('./Usuario')
