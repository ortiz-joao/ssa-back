const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLFloat } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;
const TipoAtividadeEnumType = require('../scalar_types/TipoAtividadeEnum')
const PresencaPapelEnumType = require('../scalar_types/PapelPresencaEnum')
const PresencaEstadoEnumType = require('../scalar_types/EstadoPresencaEnum')

module.exports = new GraphQLObjectType({
  name: 'Dia',
  fields: () => ({
    data: {
      type: GraphQLString,
      resolve(dia) {
        return dia.data
      }
    },
    dataString: {
      type: GraphQLString,
      resolve(dia) {
        return dia.dataString
      }
    },
    dia: {
      type: GraphQLInt,
      resove(dia) {
        return dia.dia
      }
    },
    mes: {
      type: GraphQLInt,
      resove(dia) {
        return dia.mes
      }
    },
    ano: {
      type: GraphQLInt,
      resolve(dia) {
        return dia.ano
      }
    },
    diaDaSemana: {
      type: GraphQLInt,
      resolve(dia) {
        return dia.diaDaSemana
      }
    },
    diasNoMes: {
      type: GraphQLInt,
      resolve(dia) {
        return dia.diasNoMes
      }
    },
    feriado: {
      type: GraphQLInt,
      resolve(dia) {
        return dia.feriado
      }
    },
    nomeDoFeriado: {
      type: GraphQLString,
      resolve(dia) {
        return dia.nomeDoFeriado
      }
    },
    letivo: {
      type: GraphQLInt,
      resolve(dia) {
        return dia.letivo
      }
    },
    feriasEscolares: {
      type: GraphQLInt,
      resolve(dia) {
        return dia.feriasEscolares
      }
    },
    presencas: {
      type: GraphQLList(PresencaType),
      args: {
        papel: {
          type: PresencaPapelEnumType
        },
        estado: {
          type: PresencaEstadoEnumType
        },
        observacao: {
          type: GraphQLString
        },
        nota: {
          type: GraphQLFloat
        },
        usuarioId: {
          type: GraphQLInt
        },
        atividadeId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Dia.findByPk(parent.data).then(dia => {
          return dia.getPresencasDoDia({where: args})
        })
      }
    },
    atividadesDisponiveisPorCategoria: {
      type: GraphQLList(AtividadeType),
      args: {
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString,
        },
        horarioInicio: {
          type: GraphQLString,
        },
        horarioTermino: {
          type: GraphQLString,
        },
        tipo: {
          type: TipoAtividadeEnumType,
        },
        categoriaId: {
          type: GraphQLInt
        },
      },
        resolve(parent, args){
          const searchFields = (args, id) => {
            let fields = {}
            for(let key in args) {
              if(args.hasOwnProperty(key)){
                if(args[key] !== null && key != id) {
                  fields[key] = args[key]
                }
              }
            }
            return fields
          }
          return CategoriaDeUsuario.findByPk(args.categoriaId).then(categoriaDeUsuario => {
            return categoriaDeUsuario.getAtividadesDisponiveis({where: {...searchFields(args,"categoriaId"),data: parent.data}})
          })
        }
    },
    atividadesDisponiveis: {
      type: GraphQLList(AtividadeType),
      args: {
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString,
        },
        horarioInicio: {
          type: GraphQLString,
        },
        horarioTermino: {
          type: GraphQLString,
        },
        tipo: {
          type: TipoAtividadeEnumType,
        }
      },
	     resolve(parent, args) {
	        return Dia.findByPk(parent.data).then(dia => {
  	         return dia.getAtividadesMarcadas({where: args})
          })
      }
    }
  })
})


const AtividadeType = require('./Atividade')
const PresencaType = require('./Presenca')
const Dia = require("../models/Dia")
const CategoriaDeUsuario = require("../models/CategoriaDeUsuario")
