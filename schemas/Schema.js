const graphql = require('graphql');
//graphqlTypes
const { GraphQLSchema, GraphQLList, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLFloat } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;
const TipoAtividadeEnumType = require('../scalar_types/TipoAtividadeEnum')
const TipoUsuarioEnumType = require('../scalar_types/TipoUsuarioEnum')
const PresencaPapelEnumType = require('../scalar_types/PapelPresencaEnum')
const PresencaEstadoEnumType = require('../scalar_types/EstadoPresencaEnum')
//graphqlSchemaStructures
const AlunoType = require("./Aluno")
const ResponsavelType = require("./Responsavel")
const AtividadeType = require("./Atividade")
const CategoriaDeUsuarioType = require("./CategoriaDeUsuario")
const DiaType = require("./Dia")
const DisciplinaType = require("./Disciplina")
const GestorType = require("./Gestor")
const PresencaType = require("./Presenca")
const ProfessorType = require("./Professor")
const SchemaType = require("./Schema")
const UsuarioType = require("./Usuario")
const PercursoType = require("./Percurso")
const GrupoType = require("./Grupo")
//sequelizeModels
const Aluno = require("../models/Aluno")
const Atividade = require("../models/Atividade")
const CategoriaDeUsuario = require("../models/CategoriaDeUsuario")
const Dia = require("../models/Dia")
const Disciplina = require("../models/Disciplina")
const Gestor = require("../models/Gestor")
const Presenca = require("../models/Presenca")
const Professor = require("../models/Professor")
const Responsavel = require("../models/Responsavel")
const Usuario = require("../models/Usuario")
const Leciona = require("../models/Leciona")
const DisponivelPara = require("../models/DisponivelPara")
const ResponsavelDoAluno = require("../models/ResponsavelDoAluno")
const Percurso = require("../models/Percurso")
const PercursosDoAluno = require("../models/PercursosDoAluno")
const PercursosDoProfessor = require("../models/PercursosDoProfessor")
const Orientadores = require("../models/Orientadores")
const Grupo = require("../models/Grupo")
const DisciplinaDaAtividade = require("../models/DisciplinaDaAtividade")
//sequelizeModelsRelations
Usuario.hasOne(Gestor);
Gestor.belongsTo(Usuario);
Usuario.hasOne(Professor);
Professor.belongsTo(Usuario);
Usuario.hasOne(Responsavel);
Responsavel.belongsTo(Usuario);
Usuario.hasOne(Aluno);
Aluno.belongsTo(Usuario);
Professor.belongsToMany(Disciplina, { as: "disciplinas", through: Leciona});
Disciplina.belongsToMany(Professor, {as: "professores", through: Leciona});
Aluno.belongsToMany(Responsavel, {as: "meusResponsaveis", through: ResponsavelDoAluno});
Responsavel.belongsToMany(Aluno, {as: "responsavelPor", through: ResponsavelDoAluno});
Atividade.belongsToMany(CategoriaDeUsuario, {as: "visivelPara", through: DisponivelPara});
CategoriaDeUsuario.belongsToMany(Atividade, {as: "atividadesDisponiveis", through: DisponivelPara});
Usuario.belongsToMany(Atividade, {through: Presenca});
Atividade.belongsToMany(Usuario, {through: Presenca});
Dia.hasMany(Atividade, {as: 'atividadesMarcadas', foreignKey: 'data' });
Atividade.belongsTo(Dia, { foreignKey: 'data' });
Dia.hasMany(Presenca, { as: 'PresencasDoDia', foreignKey: 'data'})
Presenca.belongsTo(Dia, {foreignKey: 'data'})
Atividade.belongsToMany(Disciplina, {as: 'materia', through: DisciplinaDaAtividade})
Disciplina.belongsToMany(Atividade, {as: 'atividadesDaDisciplina', through: DisciplinaDaAtividade})
Usuario.belongsTo(CategoriaDeUsuario, {foreignKey: 'categoria'})
CategoriaDeUsuario.hasMany(Usuario, {as: 'usuarios', foreignKey: 'categoria'})
Percurso.belongsToMany(Aluno, {through: PercursosDoAluno})
Aluno.belongsToMany(Percurso, {through: PercursosDoAluno})
Professor.belongsToMany(Percurso, {as: "responsavelPelosPercursos",through: PercursosDoProfessor})
Percurso.belongsToMany(Professor, {as: "responsaveisDoPercurso",through: PercursosDoProfessor})
Atividade.belongsTo(Percurso, {as: "percursoDaAtividade",foreignKey: 'percurso'})
Percurso.hasMany(Atividade, {as: 'atividadesDoPercurso', foreignKey: 'percurso'})
Grupo.hasMany(Aluno, {foreignKey: 'grupo'})
Aluno.belongsTo(Grupo, {as: 'meuGrupo', foreignKey: 'grupo'})
Professor.belongsToMany(Grupo, {as: 'orientandos',through: Orientadores})
Grupo.belongsToMany(Professor, {as: 'orientadores', through: Orientadores})

//updatefields
const updateFields = (args, id) => {
  let fields =[]
  for(let key in args) {
    if(args.hasOwnProperty(key)){
      if(args[key] !== null && key != id) {
        fields.push(key)
      }
    }
  }
  return fields
}
//rootQueries

const RootQuery = new GraphQLObjectType({
  name:'RootQueryType',
  fields:{
    alunos: {
      type: GraphQLList(AlunoType),
      args: {
        id: {
          type: GraphQLInt
        },
        turma: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Aluno.findAll({where: args});
      }
    },
    professores: {
      type: GraphQLList(ProfessorType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Professor.findAll({where: args});
      }
    },
    percursos: {
      type: GraphQLList(PercursoType),
      args: {
        id: {
          type: GraphQLInt
        },
        descricao: {
          type: GraphQLString
          }
        },
        resolve(parent, args) {
          return Percurso.findAll({where: args})
        }
    },
    gestores: {
      type: GraphQLList(GestorType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Gestor.findAll({where: args});
      }
    },
    grupos: {
      type: GraphQLList(GrupoType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Grupo.findAll({where: args});
      }
    },
    usuarios: {
      type: GraphQLList(UsuarioType),
      args: {
        id: {
          type: GraphQLInt
        },
        nomeDeUsuario: {
          type: GraphQLString
        },
        nomeCompleto: {
          type: GraphQLString
        },
        tipo: {
          type: TipoUsuarioEnumType
        },
        email: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Usuario.findAll({where: args});
      }
    },
    responsaveis: {
      type: GraphQLList(ResponsavelType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Responsavel.findAll({where: args});
      }
    },
    atividades: {
      type: GraphQLList(AtividadeType),
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        },
        horarioInicio: {
          type: GraphQLString
        },
        horarioTermino: {
          type: GraphQLString
        },
        tipo: {
          type: TipoAtividadeEnumType
        }
      },
      resolve(parent, args) {
        return Atividade.findAll({where: args})
      }
    },
    categoriasDeUsuarios: {
      type: GraphQLList(CategoriaDeUsuarioType),
      args: {
        id: {
          type: GraphQLInt
        },
        categoria: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.findAll({where: args})
      }
    },
    disciplinas: {
      type: GraphQLList(DisciplinaType),
      args: {
        id: {
          type: GraphQLInt
        },
        disciplina: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Disciplina.findAll({where: args})
      }
    },
    presencas: {
      type: GraphQLList(PresencaType),
      args: {
        papel:{
          type: PresencaPapelEnumType
        },
        estado: {
          type: PresencaEstadoEnumType
        },
        observacao: {
          type: GraphQLString
        },
        nota: {
          type: GraphQLFloat
        },
        data: {
          type: GraphQLString
        },
        usuarioId: {
          type: GraphQLInt
        },
        atividadeId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Presenca.findAll({where: args})
      }
    },
    dias: {
      type: GraphQLList(DiaType),
      args: {
        data: {
          type: GraphQLDate
        },
        dataString: {
          type: GraphQLString
        },
        dia: {
          type: GraphQLInt
        },
        mes: {
          type: GraphQLInt
        },
        ano: {
          type: GraphQLInt
        },
        diaDaSemana: {
          type: GraphQLInt
        },
        diasNoMes: {
          type: GraphQLInt
        },
        feriado: {
          type: GraphQLInt
        },
        nomeDoFeriado: {
          type: GraphQLString
        },
        letivo: {
          type: GraphQLInt
        },
        feriasEscolares: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Dia.findAll({where: args})
      }
    },
    aluno: {
      type: AlunoType,
      args: {
        id: {
          type: GraphQLInt
        },
        turma: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Aluno.findOne({where: args});
      }
    },
    professor: {
      type: ProfessorType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Professor.findOne({where: args});
      }
    },
    grupo: {
      type: GraphQLList(GrupoType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Grupo.findOne({where: args});
      }
    },
    gestor: {
      type: GestorType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Gestor.findOne({where: args});
      }
    },
    usuario: {
      type: UsuarioType,
      args: {
        id: {
          type: GraphQLInt
        },
        nomeDeUsuario: {
          type:GraphQLString
        },
        nomeCompleto: {
          type: GraphQLString
        },
        tipo: {
          type: TipoUsuarioEnumType
        },
        email: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Usuario.findOne({where: args});
      }
    },
    responsavel: {
      type: ResponsavelType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Responsavel.findOne({where: args});
      }
    },
    atividade: {
      type: AtividadeType,
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        },
        horarioInicio: {
          type: GraphQLString
        },
        horarioTermino: {
          type: GraphQLString
        },
        tipo: {
          type: TipoAtividadeEnumType
        }
      },
      resolve(parent, args) {
        return Atividade.findOne({where: args})
      }
    },
    categoriaDeUsuario: {
      type: CategoriaDeUsuarioType,
      args: {
        id: {
          type: GraphQLInt
        },
        categoria: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.findOne({where: args})
      }
    },
    disciplina: {
      type: DisciplinaType,
      args: {
        id: {
          type: GraphQLInt
        },
        disciplina: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Disciplina.findOne({where: args})
      }
    },
    presenca: {
      type: PresencaType,
      args: {
        papel:{
          type: PresencaPapelEnumType
        },
        estado: {
          type: PresencaEstadoEnumType
        },
        observacao: {
          type: GraphQLString
        },
        nota: {
          type: GraphQLFloat
        },
        data: {
          type: GraphQLString
        },
        atividadeId: {
          type: GraphQLInt
        },
        usuarioId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Presenca.findone({where: args})
      }
    },
    dia: {
      type: DiaType,
      args: {
        data: {
          type: GraphQLDate
        },
        dataString: {
          type: GraphQLString
        },
        mes: {
          type: GraphQLInt
        },
        ano: {
          type: GraphQLInt
        },
        diaDaSemana: {
          type: GraphQLInt
        },
        diasNoMes: {
          type: GraphQLInt
        },
        feriado: {
          type: GraphQLInt
        },
        nomeDoFeriado: {
          type: GraphQLString
        },
        letivo: {
          type: GraphQLInt
        },
        feriasEscolares: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Dia.findOne({where: args})
      }
    },
    percurso: {
      type: GraphQLList(PercursoType),
      args: {
        id: {
          type: GraphQLInt
        },
        descricao: {
          type: GraphQLString
          }
        },
        resolve(parent, args) {
          return Percurso.findOne({where: args})
        }
    }
  }
})

//mutations
const Mutations = new GraphQLObjectType({
  name:'Mutation',
  fields: {
    addDia: {
      type: DiaType,
      args: {
        data: {
          type: GraphQLString
        },
        dataString: {
          type: GraphQLString
        },
        dia: {
          type: GraphQLInt
        },
        mes: {
          type: GraphQLInt
        },
        ano: {
          type: GraphQLInt
        },
        diaDaSemana: {
          type: GraphQLInt
        },
        diasNoMes: {
          type: GraphQLInt
        },
        feriado: {
          type: GraphQLInt
        },
        nomeDoFeriado: {
          type: GraphQLString
        },
        letivo: {
          type: GraphQLInt
        },
        feriasEscolares: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Dia.create({
          data: args.data,
          dataString: args.dataString,
          dia: args.dia,
          mes: args.mes,
          ano: args.ano,
          diaDaSemana: args.diaDaSemana,
          diasNoMes: args.diasNoMes,
          feriado: args.feriado,
          letivo: args.letivo,
          feriasEscolares: args.feriasEscolares,
          nomeDoFeriado: args.nomeDoFeriado
        })
      }
    },
    addAtividade: {
      type: AtividadeType,
      args: {
        data: {
          type: GraphQLString
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        },
        horarioInicio: {
          type: GraphQLString
        },
        horarioTermino: {
          type: GraphQLString
        },
        tipo: {
          type: TipoAtividadeEnumType
        }
      },
      resolve(parent, args) {
        Atividade.create({
          nome: args.nome,
          descricao: args.descricao,
          horarioInicio: args.horarioInicio,
          horarioTermino: args.horarioTermino,
          tipo: args.tipo,
          data: args.data
        }).then(atividade => {
          Dia.findByPk(args.data).then(dia => {
            console.log(dia)
            // dia.addAtividade(atividade)
          })
        })
      }
    },
    addUsuario: {
      type: UsuarioType,
      args: {
        tipo: {
          type: GraphQLString
        },
        nomeDeUsuario: {
          type:GraphQLString
        },
        nomeCompleto: {
          type: GraphQLString
        },
        email: {
          type: GraphQLString
        },
        senha: {
          type: GraphQLString
        },
        categoriaDeUsuarioId: {
          type: GraphQLInt
        },
        turma: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        Usuario.create({
          tipo: args.tipo,
          nomeDeUsuario: args.nomeDeUsuario,
          nomeCompleto: args.nomeCompleto,
          email: args.email,
          senha: args.senha,
        }).then(usuario => {
          CategoriaDeUsuario.findByPk(args.categoriaDeUsuarioId).then(categoriaDeUsuario => {
            usuario.setCategoriaDeUsuario(categoriaDeUsuario).then(categoria => {
              if (args.tipo === "aluno") {
                usuario.createAluno({ turma: args.turma })
              } else if (args.tipo === "professor") {
                usuario.createProfessor()
              } else if (args.tipo === "responsavel") {
                usuario.createResponsavel()
              } else if (args.tipo === "gestor") {
                usuario.createGestor()
              }
            })
          })
        })
      }
    },
    addDisciplina: {
      type: DisciplinaType,
      args: {
        disciplina: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        Disciplina.create({
          disciplina: args.disciplina
        })
      }
    },
    addCategoriaDeUsuario: {
      type: CategoriaDeUsuarioType,
      args: {
        categoria: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        CategoriaDeUsuario.create({
          categoria: args.categoria
        }).then(categoria => {
          return categoria
        })
      }
    },
    addGrupo: {
      type: GrupoType,
      resolve(parent) {
        Grupo.create()
      }
    },
    addPercurso: {
      type: PercursoType,
      args: {
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        Percurso.create({
          nome: args.nome,
          descricao: args.descricao
        })
      }
    },
    addDisciplinaToProfessor: {
      type: ProfessorType,
      args: {
        professorId:{
          type: GraphQLInt
        },
        disciplinaId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Professor.findByPk(args.professorId).then(professor => {
          return Disciplina.findByPk(args.disciplinaId).then(disciplina => {
            professor.addDisciplina(disciplina).then(a => {
              console.log(a)
              return professor
            })
          })
        })
      }
    },
    addDisciplinaToAtividade: {
      type: ProfessorType,
      args: {
        atividadeId:{
          type: GraphQLInt
        },
        disciplinaId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Atividade.findByPk(args.atividadeId).then(atividade => {
            return atividade.addMateria(args.disciplinaId)
        })
      }
    },
    addResponsavelToAluno: {
      type: AlunoType,
      args: {
        alunoId: {
          type: GraphQLInt
        },
        responsavelId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Aluno.findByPk(args.alunoId).then(aluno => {
          return Responsavel.findByPk(args.responsavelId).then(responsavel => {
            responsavel.addResponsavelPor(aluno).then(a => {
              console.log(a)
              return aluno
            })
          })
        })
      }
    },
    addCategoriaDeUsuarioToAtividade: {
      type: AtividadeType,
      args: {
        atividadeId: {
          type: GraphQLInt
        },
        categoriaDeUsuarioId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Atividade.findByPk(args.atividadeId).then( atividade => {
          return CategoriaDeUsuario.findByPk(args.categoriaDeUsuarioId).then(categoria => {
            atividade.addVisivelPara(categoria).then(a => {
              console.log(a)
              return atividade
            })
          })
        })
      }
    },
    addPresencaToUsuario: {
      type: PresencaType,
      args: {
        papel: {
          type: PresencaPapelEnumType
        },
        estado: {
          type: PresencaEstadoEnumType
        },
        observacao: {
          type: GraphQLString
        },
        nota: {
          type: GraphQLFloat
        },
        usuarioId: {
          type: GraphQLInt
        },
        atividadeId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Atividade.findByPk(args.atividadeId).then(atividade => {
          return Usuario.findByPk(args.usuarioId).then(usuario => {
            atividade.addUsuario(usuario, {through:{...args, data: atividade.data}}).then(presenca => {
              return presenca
            })
          })
        })
      }
    },
    addPercursoToAluno: {
      type: PercursoType,
      args: {
        percursoId: {
          type: GraphQLInt
        },
        alunoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Percurso.findByPk(args.percursoId).then(percurso => {
          return Aluno.findByPk(args.alunoId).then(aluno => {
            percurso.addAluno(aluno).then(a => {
              console.log(a)
              return percurso
            })
          })
        })
      }
    },
    addPercursoToProfessor: {
      type: PercursoType,
      args: {
        percursoId: {
          type: GraphQLInt
        },
        professorId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Percurso.findByPk(args.percursoId).then(percurso => {
          return Professor.findByPk(args.professorId).then(professor => {
            percurso.addResponsaveisDoPercurso(professor).then(a => {
              console.log(a)
              return percurso
            })
          })
        })
      }
    },
    addAtividadeToPercurso: {
      type: PercursoType,
      args: {
        percursoId: {
          type: GraphQLInt
        },
        atividadeId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Percurso.findByPk(args.percursoId).then(percurso => {
          return Atividade.findByPk(args.atividadeId).then(atividade => {
            percurso.addAtividadesDoPercurso(atividade).then(a => {
              console.log(a)
              return percurso
            })
          })
        })
      }
    },
    addAlunoToGrupo: {
      type: GrupoType,
      args: {
        alunoId: {
          type: GraphQLInt
        },
        grupoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Grupo.findByPk(args.grupoId).then(grupo => {
          return Aluno.findByPk(args.alunoId).then(aluno => {
            aluno.setMeuGrupo(grupo).then(grupo => {
              return grupo
            })
          })
        })
      }
    },
    addProfessorToGrupo: {
      type: GrupoType,
      args: {
        professorId: {
          type: GraphQLInt
        },
        grupoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        Grupo.findByPk(args.grupoId).then(grupo => {
          return Professor.findByPk(args.professorId).then(professor => {
            professor.addOrientandos(grupo).then(grupo => {
              return grupo
            })
          })
        })
      }
    },
    updateUsuarioCategoria: {
      type: UsuarioType,
      args: {
        usuarioId: {
          type: GraphQLInt
        },
        categoriaId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Usuario.findByPk(args.usuarioId) .then(usuario => {
          return usuario.getCategoriaDeUsuario().then(categoria => {
            return categoria.removeUsuarios(usuario).then(categoria => {
              return usuario.setCategoriaDeUsuario(args.categoriaId).then(c => {
                return usuario
              })
            })
          })
        })
      }
    },
    updateDia:{
      type: DiaType,
      args: {
        data: {
          type: GraphQLString
        },
        dataString: {
          type: GraphQLString
        },
        dia: {
          type: GraphQLInt
        },
        mes: {
          type: GraphQLInt
        },
        ano: {
          type: GraphQLInt
        },
        diaDaSemana: {
          type: GraphQLInt
        },
        diasNoMes: {
          type: GraphQLInt
        },
        feriado: {
          type: GraphQLInt
        },
        nomeDoFeriado: {
          type: GraphQLString
        },
        letivo: {
          type: GraphQLInt
        },
        feriasEscolares: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        let fields = []
        for(let key in args) {
          if(args.hasOwnProperty(key)){
            if(args[key] !== null && key != "data") {
              fields.push(key)
            }
          }
        }
        return Dia.findByPk(args.data).then(dia => {
          return dia.update(args,fields).then(dia => {
            return dia
          })
        })
      }
    },
    updateUsuario: {
      type: UsuarioType,
      args: {
        id: {
          type: GraphQLInt
        },
        nomeDeUsuario: {
          type: GraphQLString
        },
        nomeCompleto: {
          type: GraphQLString
        },
        email: {
          type: GraphQLString
        },
        data: {
          type: GraphQLString
        }
      },
      resolve(parent, args){
        let fields =[]
        for(let key in args) {
          if(args.hasOwnProperty(key)){
            if(args[key] !== null && key != "id") {
              fields.push(key)
            }
          }
        }
        return Usuario.findByPk(args.id).then(usuario => {
          return usuario.update(args, fields).then(usuario => {
            return usuario
          })
        })
      }
    },
    updateAluno: {
      type: AlunoType,
      args: {
        id: {
          type: GraphQLInt
        },
        turma: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Aluno.findByPk(args.id).then(aluno => {
          return aluno.update(args, updateFields(args, "id")).then(aluno => {
            return aluno
          })
        })
      }
    },
    updateAtividade: {
      type: AtividadeType,
      args: {
        data: {
          type: GraphQLString
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        },
        horarioInicio: {
          type: GraphQLString
        },
        horarioTermino: {
          type: GraphQLString
        },
        tipo: {
          type: TipoAtividadeEnumType
        }
      },
      resolve(parent, args) {
        return Atividade.findByPk(args.data).then(atividade => {
          return atividade.update(args, updateFields(args, 'data')).then(atividade => {
            return atividade
          })
        })
      }
    },
    updateDisciplina: {
      type: DisciplinaType,
      args: {
        id: {
          type: GraphQLInt
        },
        disciplina: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Disciplina.findByPk(args.id).then(disciplina => {
          return disciplina.update(args, updateFields(args, "id")).then(disciplina => {
            return disciplina
          })
        })
      }
    },
    updateCategoriaDeUsuario: {
      type: CategoriaDeUsuarioType,
      args:{
        id: {
          type: GraphQLInt
        },
        categoria: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.findByPk(args.id).then(categoria => {
          return categoria.update(args, updateFields(args, "id")).then(categoria => {
            return categoria
          })
        })
      }
    },
    updatePercurso: {
      type: PercursoType,
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Percurso.findByPk(args.id).then(percruso => {
          return percurso.update(args, updateFields(args, "id")).then(percruso => {
            return percurso
          })
        })
      }
    },
    removeAlunoFromResponsavel:{
      type: ResponsavelType,
      args: {
        responsavelId: {
          type: GraphQLInt
        },
        alunoId: {
          type:GraphQLInt
        }
      },
      resolve(parent, args) {
        return Responsavel.findByPk(args.responsavelId).then(responsavel => {
          return responsavel.removeResponsavelPor(args.alunoId)
        })
      }
    },
    removeResponsavelFromAluno: {
      type: AlunoType,
      args: {
        responsavelId: {
          type: GraphQLInt
        },
        alunoId: {
          type:GraphQLInt
        }
      },
      resolve(parent, args) {
        return Aluno.findByPk(args.alunoId).then(aluno => {
          return aluno.removeMeusResponsaveis(args.responsavelId).then(aluno => {
            return aluno
          })
        })
      }
    },
    removePercursoFromAtividade: {
      type: AtividadeType,
      args: {
        atividadeId: {
          type: GraphQLInt
        },
        percursoId: {
          type:GraphQLInt
        }
      },
      resolve(parent, args) {
        return Atividade.findByPk(args.atividadeId).then(atividade => {
          return atividade.removePercursoDaAtividade(args.percursoId).then(atividade => {
            return atividade
          })
        })
      }
    },
    removePercursoFromProfessor: {
      type: ProfessorType,
      args: {
        professorId: {
          type: GraphQLInt
        },
        percursoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Professor.findByPk(args.professorId).then(professor => {
          return professor.removeResponsavelPelosPercursos(args.percursoId).then(professor => {
            return professor
          })
        })
      }
    },
    removePercursoFromAluno: {
      type: AlunoType,
      args: {
        alunoId: {
          type: GraphQLInt
        },
        percursoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Aluno.findByPk(args.alunoId).then(aluno => {
          return aluno.removePercurso(args.percursoId).then(aluno => {
            return aluno
          })
        })
      }
    },
    removeUsuarioFromCategoria: {
      type: CategoriaDeUsuarioType,
      args: {
        usuarioId: {
          type: GraphQLInt
        },
        categoriaId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.findByPk(args.categoriaId).then(categoria => {
          return categoria.removeUsuarios(args.usuarioId).then(categoria => {
            return categoria
          })
        })
      }
    },
    removeDisciplinaFromProfessor: {
      type: ProfessorType,
      args: {
        professorId: {
          type: GraphQLInt
        },
        disciplinaId: {
          type:GraphQLInt
        }
      },
      resolve(parent, args) {
        return Professor.findByPk(args.professorId).then(professor => {
          return professor.removeDisciplinas(args.disciplinaId).then(professor => {
            return professor
          })
        })
      }
    },
    removeDisciplinaFromAtividade: {
      type: AtividadeType,
      args: {
        atividadeId: {
          type: GraphQLInt
        },
        disciplinaId: {
          type:GraphQLInt
        }
      },
      resolve(parent, args) {
        return Atividade.findByPk(args.atividadeId).then(atividade => {
          return atividade.removeMateria(args.disciplinaId).then(atividade => {
            return atividade
          })
        })
      }
    },
    removeCategoriaFromAtividade: {
      type: AtividadeType,
      args: {
        atividadeId: {
          type: GraphQLInt
        },
        categoriaId: {
          type:GraphQLInt
        }
      },
      resolve(parent, args) {
        return Atividade.findByPk(args.atividadeId).then(atividade => {
          return atividade.removeVisivelPara(args.categoriaId)
        })
      }
    },
    removePresencaFromUser: {
      type: UsuarioType,
      args: {
        usuarioId: {
          type: GraphQLInt
        },
        atividadeId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Usuario.findByPk(args.usuarioId).then(usuario => {
          return usuario.removeAtividade(args.atividadeId).then(usuario => {
            return usuario
          })
        })
      }
    },
    removeOrientadorFromGrupo: {
      type: ProfessorType,
      args: {
        professorId: {
          type: GraphQLInt
        },
        grupoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Professor.findByPk(args.professorId).then(professor => {
          return professor.removeOrientandos(args.grupoId).then(professor => {
            return professor
          })
        })
      }
    },
    removeAlunoFromGrupo: {
      type: AlunoType,
      args: {
        alunoId: {
          type: GraphQLInt
        },
        grupoId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Grupo.findByPk(args.grupoId).then(grupo => {
          return grupo.removeAluno(args.alunoId)
        })
      }
    },
    deleteUsuario: {
      type: UsuarioType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Usuario.destroy({where: args})
      }
    },
    deleteCategoriaDeUsuario: {
      type: CategoriaDeUsuarioType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.destroy({where: args})
      }
    },
    deleteDia:{
      type: DiaType,
      args: {
        data: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Dia.destory({where: args})
      }
    },
    deleteAtividade: {
      type: AtividadeType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Atividade.destrou({where: args})
      }
    },
    deleteDisciplina: {
      type: DisciplinaType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Disciplina.destrou({where: args})
      }
    },
    deleteGrupo: {
      type: GrupoType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Grupo.destroy({where: args})
      }
    },
    deletePercurso: {
      type: PercursoType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Percurso.destroy({where: args})
      }
    },
  }
})
module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutations
})
