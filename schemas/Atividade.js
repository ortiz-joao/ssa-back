const graphql = require('graphql');
const { GraphQLList, GraphQLFloat, GraphQLObjectType, GraphQLString, GraphQLInt } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;
const TipoAtividadeEnumType = require('../scalar_types/TipoAtividadeEnum')
const PresencaPapelEnumType = require('../scalar_types/PapelPresencaEnum')
const PresencaEstadoEnumType = require('../scalar_types/EstadoPresencaEnum')

module.exports = new GraphQLObjectType({
  name: 'Atividade',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(atividade) {
        return atividade.id
      }
    },
    nome: {
      type: GraphQLString,
      resolve(atividade) {
        return atividade.nome
      }
    },
    descricao: {
      type: GraphQLString,
      resolve(atividade) {
        return atividade.descricao
      }
    },
    horarioInicio: {
      type: GraphQLString,
      resolve(atividade) {
        return atividade.horarioInicio
      }
    },
    horarioTermino: {
      type: GraphQLString,
      resolve(atividade) {
        return atividade.horarioTermino
      }
    },
    tipo: {
      type: TipoAtividadeEnumType,
      resolve(atividade) {
        return atividade.tipo
      }
    },
    data: {
      type: GraphQLString,
      resolve(parent) {
        return parent.data
      }
    },
    dia: {
      type: DiaType,
      resolve(parent) {
        return Atividade.findByPk(parent.id).then(atividade => {
          return atividade.getDia()
        })
      }
    },
    materias: {
      type: GraphQLList(DisciplinaType),
      args: {
        id: {
          type: GraphQLInt
        },
        disciplina: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Atividade.findByPk(parent.id).then(atividade => {
          return atividade.getMateria({where: args})
        })
      }
    },
    presencas: {
      type: GraphQLList(PresencaType),
      args: {
        papel: {
          type: PresencaPapelEnumType
        },
        estado: {
          type: PresencaEstadoEnumType
        },
        observacao: {
          type: GraphQLString
        },
        nota: {
          type: GraphQLFloat
        },
        usuarioId: {
          type: GraphQLInt
        },
        data: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Presenca.findAll({where:{...args, atividadeId: parent.id}})
      }
    },
    participantes: {
      type: GraphQLList(UsuarioType),
      args: {
        id: {
          type: GraphQLInt
        },
        nomeDeUsuario: {
          type: GraphQLString
        },
        nomeCompleto: {
          type: GraphQLString
        },
        email: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Atividade.findByPk(parent.id).then(atividade => {
          return atividade.getUsuarios({where: args})
        })
      }
    },
    percursoDaAtividade: {
      type: PercursoType,
      resolve(parent) {
        return Atividade.findByPk(parent.id).then(atividade => {
          return atividade.getPercursoDaAtividade()
        })
      }
    }
  })
})

const Atividade = require("../models/Atividade")
const Presenca = require("../models/Presenca")
const DiaType = require('./Dia')
const UsuarioType = require('./Usuario')
const DisciplinaType = require('./Disciplina')
const PresencaType = require('./Presenca')
const PercursoType = require('./Percurso')
