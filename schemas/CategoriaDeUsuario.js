const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;
const TipoAtividadeEnumType = require('../scalar_types/TipoAtividadeEnum')

module.exports = new GraphQLObjectType({
  name: 'CategoriaDeusuario',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(categoriaDeUsuario) {
        return categoriaDeUsuario.id
      }
    },
    categoria: {
      type: GraphQLString,
      resolve(categoriaDeUsuario) {
         return categoriaDeUsuario.categoria
      }
    },
    atividadesDisponiveis:{
      type: GraphQLList(AtividadeType),
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString,
        },
        horarioInicio: {
          type: GraphQLDateTime,
        },
        horarioTermino: {
          type: GraphQLDateTime,
        },
        tipo: {
          type: TipoAtividadeEnumType,
        },
        data: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.findByPk(parent.id).then(categoriaDeUsuario => {
          return categoriaDeUsuario.getAtividadesDisponiveis({where: args})
        })
      }
    },
    usuarios: {
      type: GraphQLList(UsuarioType),
      args: {
        id: {
          type: GraphQLInt
        },
        nomeDeUsuario: {
          type: GraphQLString
        },
        nomeCompleto: {
          type: GraphQLString
        },
        email: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return CategoriaDeUsuario.findByPk(parent.id).then(categoria => {
          return categoria.getUsuarios({where: args})
        })
      }
    }
  })
})

const Atividade = require("../models/Atividade")
const CategoriaDeUsuario = require("../models/CategoriaDeUsuario")
const AtividadeType = require('./Atividade')
const UsuarioType = require('./Usuario')
