const graphql = require('graphql');
const { GraphQLList, GraphQLObjectType, GraphQLString, GraphQLInt } = graphql;

module.exports = new GraphQLObjectType({
  name: 'Aluno',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(aluno) {
        return aluno.id;
      }
    },
    turma: {
      type: GraphQLString,
      resolve(aluno) {
        return aluno.turma
      }
    },
    meusResponsaveis:{
      type: GraphQLList(ResponsavelType),
      resolve(parent, args, context, info) {
          return Aluno.findByPk(parent.id).then(aluno => {
            return aluno.getMeusResponsaveis()
          })
      }
    },
    usuario: {
      type: UsuarioType,
      resolve(parent, args, context, info) {
          return Aluno.findByPk(parent.id).then(aluno => {
            return aluno.getUsuario()
          })
      }
    },
    grupo: {
      type: GrupoType,
      resolve(parent) {
        return Aluno.findByPk(parent.id).then(aluno => {
          return aluno.getMeuGrupo()
        })
      }
    },
    percursosDoAluno: {
      type: GraphQLList(PercursoType),
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        }
      },
      resolve(parent, args) {
        return Aluno.findByPk(parent.id).then(aluno => {
          return aluno.getPercursos({where: args})
        })
      }
    }
  })
})


const Aluno = require('../models/Aluno')
const ResponsavelType = require('./Responsavel')
const GrupoType = require('./Grupo')
const PercursoType = require('./Percurso')
const UsuarioType = require('./Usuario')
