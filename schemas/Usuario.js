const graphql = require('graphql');
const { GraphQLList, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLFloat } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;
const TipoUsuarioEnumType = require('../scalar_types/TipoUsuarioEnum')
const PresencaPapelEnumType = require('../scalar_types/PapelPresencaEnum')
const PresencaEstadoEnumType = require('../scalar_types/EstadoPresencaEnum')

module.exports = new GraphQLObjectType({
  name: 'Usuario',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(usuario) {
        return usuario.id
      }
    },
    nomeDeUsuario: {
      type: GraphQLString,
      resolve(usuario) {
        return usuario.nomeDeUsuario
      }
    },
    nomeCompleto: {
      type: GraphQLString,
      resolve(usuario) {
        return usuario.nomeCompleto
      }
    },
    tipo: {
      type: TipoUsuarioEnumType,
      resolve(usuario) {
        return usuario.tipo
      }
    },
    email: {
      type: GraphQLString,
      resolve(usuario) {
        return usuario.email
      }
    },
    senha: {
      type: GraphQLString,
      resolve(usuario) {
        return usuario.senha
      }
    },
    categoria: {
      type: CategoriaDeUsuarioType,
      resolve(parent) {
        return Usuario.findByPk(parent.id).then(usuario => {
          return usuario.getCategoriaDeUsuario()
        })
      }
    },
    responsavel: {
      type: ResponsavelType,
      resolve(parent, args, context, info) {
          return Usuario.findByPk(parent.id).then(usuario => {
            return usuario.getResponsavel()
          })
      }
    },
    aluno:{
      type: AlunoType,
      resolve(parent, args, context, info) {
          return Usuario.findByPk(parent.id).then(usuario => {
            return usuario.getAluno()
          })
      }
    },
    professor:{
      type: ProfessorType,
      resolve(parent, args, context, info) {
          return Usuario.findByPk(parent.id).then(usuario => {
            return usuario.getProfessor()
          })
      }
    },
    gestor: {
      type: GestorType,
      resolve(parent, args, context, info) {
          return Usuario.findByPk(parent.id).then(usuario => {
            return usuario.getGestor()
          })
      }
    },
    presencas: {
      type: GraphQLList(PresencaType),
      args: {
        papel: {
          type: PresencaPapelEnumType
        },
        estado: {
          type: PresencaEstadoEnumType
        },
        observacao: {
          type: GraphQLString
        },
        nota: {
          type: GraphQLFloat
        },
        data: {
          type: GraphQLString
        },
        atividadeId: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Presenca.findAll({where: {...args, usuarioId: parent.id}})
      }
    },
    atividades: {
      type: GraphQLList(AtividadeType),
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString,
        },
        horarioInicio: {
          type: GraphQLDateTime,
        },
        horarioTermino: {
          type: GraphQLDateTime,
        },
        data: {
          type: GraphQLDate
        }
      },
      resolve(parent, args, context, info) {
          return Usuario.findByPk(parent.id).then(usuario => {
            return usuario.getAtividades({where: args}).then()
          })
      }
    }
  })
})


const CategoriaDeUsuarioType = require('./CategoriaDeUsuario')
const ProfessorType = require('./Professor')
const ResponsavelType = require('./Responsavel')
const PresencaType = require('./Presenca')
const GestorType = require('./Gestor')
const AtividadeType = require('./Atividade')
const Usuario = require("../models/Usuario")
const Presenca = require("../models/Presenca")
const AlunoType = require('./Aluno')
