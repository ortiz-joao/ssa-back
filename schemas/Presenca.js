const graphql = require('graphql');
const { GraphQLFloat, GraphQLObjectType, GraphQLString, GraphQLInt } = graphql;
const graphqlIsoDate = require('graphql-iso-date');
const { GraphQLDate, GraphQLDateTime } = graphqlIsoDate;
const PresencaPapelEnumType = require('../scalar_types/PapelPresencaEnum')
const PresencaEstadoEnumType = require('../scalar_types/EstadoPresencaEnum')

module.exports = new GraphQLObjectType({
  name: 'Presenca',
  fields: () => ({
    papel: {
      type: PresencaPapelEnumType,
      resolve(presenca) {
         return presenca.papel
      }
    },
    estado: {
      type: PresencaEstadoEnumType,
      resolve(presenca) {
        return presenca.estado
      }
    },
    observacao: {
      type: GraphQLString,
      resolve(presenca) {
        return presenca.observacao
      }
    },
    nota: {
      type: GraphQLFloat,
      resolve(presenca) {
         return presenca.nota
      }
    },
    usuario: {
      type: UsuarioType,
      resolve(parent) {
        return Usuario.findByPk(parent.usuarioId)
      }
    },
    atividade: {
      type: AtividadeType,
      resolve(parent) {
        return Atividade.findByPk(parent.atividadeId)
      }
    },
  })
})

const Atividade = require("../models/Atividade")
const Usuario = require("../models/Usuario")
const AtividadeType = require('./Atividade')
const UsuarioType = require('./Usuario')
