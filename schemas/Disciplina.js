const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList } = graphql;
const TipoAtividadeEnumType = require('../scalar_types/TipoAtividadeEnum')

module.exports = new GraphQLObjectType({
  name: 'Disciplina',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(disciplina) {
        return disciplina.id
      }
    },
    disciplina: {
      type: GraphQLString,
      resolve(disciplina) {
        return disciplina.disciplina
      }
    },
    professores:{
      type: GraphQLList(ProfessorType),
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parent, args) {
        return Disciplina.findByPk(parent.id).then(disciplina => {
          return disciplina.getProfessores({where: args})
        })
      }
    },
    atividadesDaDisciplina: {
      type: GraphQLList(AtividadeType),
      args: {
        id: {
          type: GraphQLInt
        },
        nome: {
          type: GraphQLString
        },
        descricao: {
          type: GraphQLString
        },
        horarioInicio: {
          type: GraphQLString
        },
        horarioTermino: {
          type: GraphQLString
        },
        tipo: {
          type: TipoAtividadeEnumType
        }
      },
      resolve(parent){
        return Disciplina.findByPk(parent.id).then(disciplina => {
          return disciplina.getAtividadesDaDisciplina({where: args})
        })
      }
    }
  })
})

const Disciplina = require('../models/Disciplina')
const ProfessorType = require('./Professor')
const AtividadeType = require('./Atividade')
