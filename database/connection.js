const Sequelize = require("sequelize");

const Conn = new Sequelize("teste", "root", "root", {
  host: "127.0.0.1",
  dialect: "mysql",
  operatorsAliases: false
});

Conn.authenticate()
  .then(() => console.log("Connection has been established successfully."))
  .catch(err => console.log("Unable to connect to the database: ", err));

module.exports = Conn;
