const express = require('express');
const graphqlHTTP = require('express-graphql')
const schema = require('./schemas/Schema')
const app = express();

app.listen(4000, () => {
  console.log('now listening to radio 40.00');
});

//DB Connection
const Conn = require("./database/connection");
//create tables and relations
//graphqlMiddlware
app.use('/graphql', graphqlHTTP({
  schema,
  pretty: true,
  graphiql: true
}))
// initModels
const Aluno = require("./models/Aluno")
const Atividade = require("./models/Atividade")
const CategoriaDeUsuario = require("./models/CategoriaDeUsuario")
const Dia = require("./models/Dia")
const Disciplina = require("./models/Disciplina")
const Gestor = require("./models/Gestor")
const Presenca = require("./models/Presenca")
const Professor = require("./models/Professor")
const Responsavel = require("./models/Responsavel")
const Usuario = require("./models/Usuario")
const Leciona = require("./models/Leciona")
const DisponivelPara = require("./models/DisponivelPara")
const ResponsavelDoAluno = require("./models/ResponsavelDoAluno")
const Percurso = require("./models/Percurso")
const PercursosDoAluno = require("./models/PercursosDoAluno")
const PercursosDoProfessor = require("./models/PercursosDoProfessor")
const Orientadores = require("./models/Orientadores")
const Grupo = require("./models/Grupo")
const DisciplinaDaAtividade = require("./models/DisciplinaDaAtividade")
// // relations
// Usuario.hasOne(Gestor);
// Gestor.belongsTo(Usuario);
// Usuario.hasOne(Professor);
// Professor.belongsTo(Usuario);
// Usuario.hasOne(Responsavel);
// Responsavel.belongsTo(Usuario);
// Usuario.hasOne(Aluno);
// Aluno.belongsTo(Usuario);
// Professor.belongsToMany(Disciplina, { as: "disciplinas", through: Leciona});
// Disciplina.belongsToMany(Professor, {as: "professores", through: Leciona});
// Aluno.belongsToMany(Responsavel, {as: "meusResponsaveis", through: ResponsavelDoAluno});
// Responsavel.belongsToMany(Aluno, {as: "responsavelPor", through: ResponsavelDoAluno});
// Atividade.belongsToMany(CategoriaDeUsuario, {as: "visivelPara", through: DisponivelPara});
// CategoriaDeUsuario.belongsToMany(Atividade, {as: "atividadesDisponiveis", through: DisponivelPara});
// Usuario.belongsToMany(Atividade, {through: Presenca});
// Atividade.belongsToMany(Usuario, {through: Presenca});
// Dia.hasMany(Atividade, {as: 'atividadesMarcadas', foreignKey: 'data' });
// Atividade.belongsTo(Dia, { foreignKey: 'data' });
// Dia.hasMany(Presenca, { as: 'presencasDoDia', foreignKey: 'data'})
// Presenca.belongsTo(Dia, {foreignKey: 'data'})
// Atividade.belongsToMany(Disciplina, {as: 'materia', through: DisciplinaDaAtividade})
// Disciplina.belongsToMany(Atividade, {as: 'atividadesDaDisciplina', through: DisciplinaDaAtividade})
// Usuario.belongsTo(CategoriaDeUsuario, {foreignKey: 'categoria'})
// CategoriaDeUsuario.hasMany(Usuario, {as: 'usuarios', foreignKey: 'categoria'})
// Percurso.belongsToMany(Aluno, {through: PercursosDoAluno})
// Aluno.belongsToMany(Percurso, {through: PercursosDoAluno})
// Professor.belongsToMany(Percurso, {as: "responsavelPelosPercursos",through: PercursosDoProfessor})
// Percurso.belongsToMany(Professor, {as: "responsaveisDoPercurso",through: PercursosDoProfessor})
// Atividade.belongsTo(Percurso, {as: "percursoDaAtividade",foreignKey: 'percurso'})
// Percurso.hasMany(Atividade, {as: 'atividadesDoPercurso', foreignKey: 'percurso'})
// Grupo.hasMany(Aluno, {foreignKey: 'grupo'})
// Aluno.belongsTo(Grupo, {as: 'meuGrupo', foreignKey: 'grupo'})
// Professor.belongsToMany(Grupo, {as: 'orientandos',through: Orientadores})
// Grupo.belongsToMany(Professor, {as: 'orientadores', through: Orientadores})

// Conn.sync({ force: true })
const errHandler = (err) => {
  console.error("Erro", err)
  console.log(err.sql)
}
// const meda = async () => {
  //forceDatabaseDropIfExistsAndCreate
  // Conn.sync({ force: true })
  // Dia.sync({ force: true })
  // Atividade.sync({ force: true })
  // Usuario.sync({ force: true })
  // Aluno.sync({ force: true })
  // Responsavel.sync({ force: true })
  // Gestor.sync({ force: true })
  // Disciplina.sync({ force: true })
  // Professor.sync({ force: true })
  // Leciona.sync({ force: true })
  // CategoriaDeUsuario.sync({ force: true })
  // Presenca.sync({ force: true })
  // DisponivelPara.sync({ force: true })
  // ResponsavelDoAluno.sync({ force: true })
  // console.log("meda")

// }
const meda1 = async () => {
  // const dia = await Dia.findOne({where:{data : "2000-08-09"}}).catch(errHandler).then(dia => {
    // return dia.getAtividadesMarcadas({raw: true}).catch(errHandler).then(atividades => {
  //     console.log(atividades)
  //     return atividades
  //   })
  // })
  // const atividade = Atividade.findOne({where:{id:'3'}}).catch(errHandler).then(atividade => {
  //   return atividade.getVisivelPara({raw: true})
  // }).catch(errHandler).then(categorias => {
  //   console.log(categorias)
  //   return categorias
  // })
  // const atividade = Atividade.findOne({where:{id:'3'}}).catch(errHandler).then(atividade => {
  //   return atividade.addVisivelPara("2")
  // })
  // const categoriaDeUsuario = CategoriaDeUsuario.bulkCreate([{
  //   categoria: "todos"
  // },{
  //   categoria: "quinto ano"
  // }, {
  //   categoria: "sexto ano"
  // },{
  //   categoria: "professores"
  // },{
  //   categoria: "pais"
  // }]).catch(errHandler).then(categorias => {
  //   console.log(categorias)
  //   return categorias
  // })
  // const atividade = Atividade.findOne({where:{id:'3'}}).catch(errHandler).then(atividade => {
  //   Usuario.findOne({where:{id:'2'}}).catch(errHandler).then(usuario => {
  //   return atividade.addUsuario(usuario, {through:{papel:"participante",estado:"pretenteAtender"}}).catch(errHandler).then(presenca => {
  //       return presenca
  //     })
  //   })
  // })
  // const responsavel = await Responsavel.findOne({where:{usuarioId: "3"}}).catch(errHandler).then(responsavel => {
  //     console.log(responsavel)
  //     return responsavel.getResponsavelPor()
  //   }).catch(errHandler).then(alunos => {
  //     console.log(alunos)
  //     return alunos
  //   })
  // const aluno = await Aluno.findOne({where:{usuarioId: "2"}}).catch(errHandler).then(aluno => {
  //   return Responsavel.findOne({where:{usuarioId: "3"}}).catch(errHandler).then(responsavel => {
  //     console.log(responsavel)
  //     responsavel.addResponsavelPor(aluno)
  //   })
  // })
  // const user = await Usuario.findOne({where:{ id: 1}}).catch(errHandler).then(usuario => {
  //   return usuario.getProfessor()
  // }).catch(errHandler).then(professor => {
  //   return professor.getDisciplinas()
  // }).catch(errHandler).then(disciplinas => {
  //   console.log(disciplinas)
  //   return disciplinas
  // })
  // const args = {
  //   nomeCompleto: null,
  //   email: "medateucukkkk@email.com"
  // }
  // let fields =[]
  // for(let key in args) {
  //   if(args.hasOwnProperty(key)){
  //     if(args[key] !== null) {
  //       fields.push(key)
  //     }
  //   }
  // }
  // const user = await Usuario.findOne({where:{ id: 1}}).catch(errHandler).then(usuario => {
  //   return usuario.update(args, fields)
  // })
  //   const disciplina = Disciplina.findOne({where: {disciplina: "artes"}}).catch(errHandler).then(disciplina => {
  //     return professor.addDisciplina(disciplina).catch(errHandler).then(leciona => {
  //       return leciona
  //     })
  //   })
  // })
  // const disciplinas = await Disciplina.findOne({where:{disciplina: "artes"}}).catch(errHandler).then(disciplina => {
  //   return disciplina.getProfessores({raw: true})
  // }).catch(errHandler).then(professores => {
  //   console.log(professores)
  //   return professores
  // })
  // const disciplina = await Disciplina.create({disciplina: 'artes'}).catch(errHandler).then(disciplina => {
  //   return disciplina
  // })
  // const users = await Usuario.findAll({raw: true}).catch(errHandler).then(user => {
  //   console.log(user)
  //   return user
  // })
  // const atividade = await Atividade.create({
  //   nome: "atividademeda",
  //   descricao: "atividade muito foda mano saudades da minha g",
  //   horarioInicio: Date(),
  //   horarioTermino: Date(),
  //   tipo: "prova",
  //   data: '2000-08-09'
  // }).catch(errHandler).then(atividade => {
  //   return atividade.addUsuario('1', {through: {papel: 'responsavel',estado: 'pretenteAtender'}})
  // }).catch(errHandler).then(presenca => {
  //   return presenca
  // })
  // const data = await Dia.create({
  //   data: '2000-08-09',
  //   dataString: '08/09/2000',
  //   dia: 9,
  //   mes: 8,
  //   ano: 2000,
  //   diaDaSemana: 4,
  //   diasNoMes: 31,
  //   feriado: 0,
  //   letivo: 1,
  //   feriasEscolares: 0
  // }).catch(errHandler)
  // const user21 = await Usuario.findByPk(1).catch(errHandler).then(user => {
  //   CategoriaDeUsuario.findByPk(1).then(c => {
  //     return user.setCategoriaDeUsuario(c)
  //   }).catch(errHandler)
  // })
  // const user1 = await Usuario.create({
  //   nomeDeUsuario: "meda311",
  //   nomeCompleto: "meda muito",
  //   tipo: "aluno",
  //   email: "me22da3a@meda.com.br",
  //   senha: "meda123",
  // }).catch(errHandler).then(user => {
  //   console.log(user)
  //   return user.createAluno({ turma: "4I"})
  // })
  // const user = await Usuario.create({
  //   nomeDeUsuario: "meda322aPai",
  //   nomeCompleto: "meda muito",
  //   tipo: "aluno",
  //   email: "med2a22a.pai@meda.com.br",
  //   senha: "meda123",
  // }).catch(errHandler).then(user => {
  //   console.log(user)
  //   return user.createResponsavel()
  // })
}
// setTimeout(() => { meda() }, 3000);
// setTimeout(() => { meda1() }, 300);
