const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

module.exports = new GraphQLEnumType({
  name: 'TipoUsuarioEnum',
  values: {
    aluno: {
      value: 'aluno',
    },
    professor: {
      value: 'professor',
    },
    responsavel: {
      value: 'responsavel',
    },
    gestor: {
      value: 'gestor',
    },
  },
});
