const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

module.exports = new GraphQLEnumType({
  name: 'PresencaPapelEnum',
  values: {
    responsavel: {
      value: 'responsavel',
    },
    participante: {
      value: 'participante',
    }
  },
});
