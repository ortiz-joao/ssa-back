const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

module.exports = new GraphQLEnumType({
  name: 'TipoAtividadeEnum',
  values: {
    aula: {
      value: 'aula',
    },
    prova: {
      value: 'prova',
    },
    atividadePessoal: {
      value: 'atividadePessoal',
    },
  },
});
