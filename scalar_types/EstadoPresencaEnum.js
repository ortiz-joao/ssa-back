const graphql = require('graphql');
const { GraphQLEnumType } = graphql;

module.exports = new GraphQLEnumType({
  name: 'PresencaEstadoEnum',
  values: {
    presente: {
      value: 'presente',
    },
    ausente: {
      value: 'ausente',
    },
    pretendeAtender: {
      value: 'pretenteAtender',
    },
  },
});
