const Sequelize = require("sequelize");
const Conn = require("../database/connection");
const Atividade = require("./Atividade");
//model
const Dia = Conn.define('dia', {
  data: {
    type: Sequelize.DATEONLY,
    allowNull: false,
    primaryKey: true
  },
  dataString: {
    type: Sequelize.STRING(10),
    allowNull: false
  },
  dia: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  mes: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  ano: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  diaDaSemana: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  diasNoMes: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  feriado: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  nomeDoFeriado: {
    type: Sequelize.STRING(50)
  },
  letivo: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  feriasEscolares: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
})
//relations


module.exports = Dia;
