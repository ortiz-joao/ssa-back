const Sequelize = require("sequelize");
const Conn = require("../database/connection");
const Presenca = require("./Presenca");
const Usuario = require("./Usuario");
const CategoriaDeUsuario = require("./CategoriaDeUsuario")
//model
const Atividade = Conn.define('atividades', {
  nome: {
    type: Sequelize.STRING(45),
    allowNull: false
  },
  descricao: {
    type: Sequelize.TEXT('tiny'),
    allowNull: false
  },
  horarioInicio: {
    type: Sequelize.DATE,
    allowNull: false
  },
  horarioTermino: {
    type: Sequelize.DATE,
    allowNull: false
  },
  tipo: {
    type: Sequelize.ENUM("aula", "prova", "atividadePessoal")
  }
});
//require



module.exports = Atividade;
