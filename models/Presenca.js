const Sequelize = require("sequelize");
const Conn = require("../database/connection");

//model
const Presenca = Conn.define('presenca', {
  papel: {
    type: Sequelize.ENUM('responsavel', 'participante'),
    allowNull: false
  },
  estado: {
    type: Sequelize.ENUM('presente', 'ausente', 'pretenteAtender'),
    allowNull: false
  },
  observacao: {
    type: Sequelize.STRING
  },
  nota: {
    type: Sequelize.FLOAT
  }
});
//relations

module.exports = Presenca;
