const Sequelize = require("sequelize");
const Conn = require("../database/connection");
const Aluno = require("./Aluno");
const Professor = require("./Professor");
const Gestor = require("./Gestor");
const Responsavel = require("./Responsavel");
//model
const Usuario = Conn.define('usuario', {
  nomeDeUsuario: {
    type: Sequelize.STRING(16),
    allowNull: false,
    unique: true
  },
  nomeCompleto: {
    type: Sequelize.STRING(60),
    allowNull: false
  },
  tipo: {
    type: Sequelize.ENUM("aluno", "professor", "responsavel", "gestor"),
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  senha: {
    type: Sequelize.STRING(60).BINARY,
    allowNull: false
  },
  local: {
    type: Sequelize.STRING(45)
  }
});
//relations



module.exports = Usuario;
