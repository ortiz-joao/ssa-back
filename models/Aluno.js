const Sequelize = require("sequelize");
const Conn = require("../database/connection");
const Usuario = require("./Usuario");
const Responsavel = require("./Responsavel");
//model
const Aluno = Conn.define('aluno', {
  turma: {
    type: Sequelize.STRING(2),
    allowNull: false
  }
});
//relations

module.exports = Aluno;
