const Sequelize = require("sequelize");
const Conn = require("../database/connection");
const Professor = require("./Professor");
//model
const Disciplina = Conn.define('disciplina', {
  disciplina: {
    type: Sequelize.STRING(50),
    allowNull: false
  }
})
//realtions

module.exports = Disciplina;
