const Sequelize = require("sequelize");
const Conn = require("../database/connection");
//model
const Percurso = Conn.define('percurso', {
  nome: {
    type: Sequelize.STRING(45),
    allowNull: false
  },
  descricao: {
    type: Sequelize.TEXT('tiny'),
    allowNull: false
  }
});
//require



module.exports = Percurso;
