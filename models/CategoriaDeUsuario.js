const Sequelize = require("sequelize");
const Conn = require("../database/connection");
//model
const CategoriaDeUsuario = Conn.define('categoriaDeUsuario', {
  categoria: {
    type: Sequelize.STRING(50),
    allowNull: false
  }
})
//realtions

module.exports = CategoriaDeUsuario;
